/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.moutoh.generatepasswordwithpassay;

import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;

/**
 *
 * @author vagrant
 */
// crée la classe GeneratePasswordExample1 pour générer un mot de passe aléatoire et sécurisé
public class GeneratePasswordTest1 {

    // démarrage de la méthode main()
    public static void main(String args[]) {
        // appelle la méthode generatePassword() pour générer un mot de passe aléatoire à l'aide de la bibliothèque Passay
        String pass = generateSecurePassword();
        // affiche le mot de passe généré par Passay
        System.out.println("Le mot de passe généré par Passay est :"+pass);
    }
    // crée la méthode generateSecurePassword() qui trouve le mot de passe sécurisé et le renvoie à la méthode main()
    public static String generateSecurePassword(){
        // crée une règle de caractère pour les minuscules
        CharacterRule LCR = new CharacterRule(EnglishCharacterData.LowerCase);  
        // définit le nombre de caractères minuscules
        LCR.setNumberOfCharacters(2);  
        // crée une règle de caractère pour les majuscules
        CharacterRule UCR = new CharacterRule(EnglishCharacterData.UpperCase);  
        // définit le nombre de caractères majuscules
        UCR.setNumberOfCharacters(2);  
        // crée une règle de caractère pour le chiffre
        CharacterRule DR = new CharacterRule(EnglishCharacterData.Digit);  
        // définit le nombre de chiffres
        DR.setNumberOfCharacters(2); 
        // crée une règle de caractère pour les minuscules
        CharacterRule SR = new CharacterRule(EnglishCharacterData.Special);  
        // définir le nombre de caractères spéciaux
        SR.setNumberOfCharacters(2);  
        // crée une instance de la classe PasswordGenerator
        PasswordGenerator passGen = new PasswordGenerator(); 
        // appelez la méthode generatePassword() de la classe PasswordGenerator pour obtenir le mot de passe généré par Passay
        String password = passGen.generatePassword(8, SR, LCR, UCR, DR);
        // renvoie le mot de passe généré par Passay à la méthode main()
        return password;  
    }
}
